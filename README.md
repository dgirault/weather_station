= Weather Station board using a Lolin32 ESP32 board =

This is a rework of a POC, I've already made using a proto board.
Protoboard don't work very well because not enough space between
the Lolin32 board and the BME280 module which become hot because 
of 3.3V regulator (ME6211) and charging component (TP4054).

This board adds 4 MOS transistors to enable disable some parts
on demand. Oled display, DME280, vbat sensing divider and RGB
leds.

A header was added to allow connected either a SPI sensors plus
3 analog inputs. A total of 7 GPIO input/output can be used.
